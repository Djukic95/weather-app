function init(){
	if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(successFunction);
     
        //Get the latitude and the longitude;
        function successFunction(position) {
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            
            sendWeatherRequest("coordinates", lat, lng);
        }
    }
    sendVisitsRequest();
}

function getWeather(){
    city = document.getElementById("city-name").value;
    if (city != "") {
        sendWeatherRequest("city", city);
        sendVisitsRequest();
    }
}

function fillTable(visits){
    var table = document.getElementsByClassName("table")[0];
    table.innerHTML = "";
    var th = document.createElement("div");
    th.classList.add("th");

    var thCity = document.createElement("div");
    thCity.classList.add("td");
    thCity.innerHTML = "City";
    var thVisits = document.createElement("div");
    thVisits.classList.add("td");
    thVisits.innerHTML = "Number of searches";

    th.appendChild(thCity);
    th.appendChild(thVisits);
    table.appendChild(th);

    var tbody = document.createElement("div");
    tbody.classList.add("tbody");
    for(var i =0;i< visits.length; i++){
        var tr = document.createElement("div");
        tr.classList.add("tr");
        var tdCity = document.createElement("div");
        tdCity.classList.add("td");
        tdCity.innerHTML = decodeURIComponent(visits[i].city);
        var tdVisits = document.createElement("div");
        tdVisits.classList.add("td");
        tdVisits.innerHTML = visits[i].number_of_visits;

        tr.appendChild(tdCity);
        tr.appendChild(tdVisits);
        tbody.appendChild(tr);
        table.appendChild(tbody);
    }
}

function showCurrentWeather(weather) {
    //console.log(weather);
    sendVisitsRequest();
    if (JSON.stringify(weather) !== JSON.stringify({})){
        var rightSide = document.getElementById("right");
        rightSide.innerHTML = "";
        for(var i=0; i< weather.length; i++)
        {
            var weatherElement = document.createElement("article");
            weatherElement.classList.add("box");
            weatherElement.classList.add("weather");

            var divClass = document.createElement("div");
            divClass.classList.add("icon");
            divClass.classList.add("bubble");
            divClass.classList.add("black");

            var divClassSpin = document.createElement("div");
            divClassSpin.classList.add("spin");
            var img = document.createElement("img");
            img.src = "https://s3-us-west-2.amazonaws.com/s.cdpn.io/162656/" + weather[i].icon + ".svg";

            divClassSpin.appendChild(img);
            divClass.appendChild(divClassSpin);

            var h1 = document.createElement("h1");
            h1.innerHTML = weather[i].city + "<br />" + weather[i].time;

            var spanTemp = document.createElement("span");
            spanTemp.classList.add("temp");
            spanTemp.innerHTML = parseInt(weather[i].curr_temp) + "&deg;";

            var description = document.createElement("span");
            description.classList.add("high-low");
            description.innerHTML = weather[i].description;

            weatherElement.appendChild(divClass);
            weatherElement.appendChild(h1);
            weatherElement.appendChild(spanTemp);
            weatherElement.appendChild(description);

            rightSide.appendChild(weatherElement);
        }
        
    }
}

function sendWeatherRequest() {    
    var req = new XMLHttpRequest();
    req.open('GET', '/api/get_weather', true);
    req.setRequestHeader("data-type", arguments[0]);
    if (arguments.length == 3) {
        req.setRequestHeader("latitude", arguments[1]);
        req.setRequestHeader("longitude", arguments[2]);
    } else {
        req.setRequestHeader("city", encodeURIComponent(arguments[1]));
    }
    req.send(null);
    req.onreadystatechange = () => {
         if (req.readyState == 4 && req.status == 200) {
            showCurrentWeather(JSON.parse(req.responseText))
        }
    }
}

function sendVisitsRequest(){
    var req = new XMLHttpRequest();
    req.open('GET', '/api/get_visits', true);
    req.send(null);
    req.onreadystatechange = () => {
         if (req.readyState == 4 && req.status == 200) {
            fillTable(JSON.parse(req.responseText))
        }
    }
}
var cities = [];
var req = new XMLHttpRequest();
req.open('POST', 'https://countriesnow.space/api/v0.1/countries/cities', true);
req.setRequestHeader('Content-Type', 'application/json');
req.send(body=JSON.stringify({"country" : "Bosnia and Herzegovina"}));
req.onreadystatechange = () => {
    if (req.readyState == 4 && req.status == 200) {
        cities= JSON.parse(req.responseText).data;
    }
}

function getCities(){
    var inputElement = document.getElementById("city-name");
    
    var currValue = inputElement.value;
    if (currValue != ""){
        var datalist = document.getElementById("cities");
        datalist.innerHTML = "";
        cities.filter(c => c.toLowerCase().startsWith(currValue.toLowerCase())).forEach(c => {
            var option = document.createElement("option");
            option.innerText = c;
            datalist.appendChild(option);
        });
    }   
}