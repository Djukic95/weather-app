import redis
import json

HOST = "localhost"
PORT = 6379
DB = 0
DECODE_RESPONSE = True

rdb = redis.Redis(host=HOST, port=PORT, db=DB, decode_responses=DECODE_RESPONSE)

def store_in_database(city):
    old_value = rdb.get(city)
    if (old_value != None):
        rdb.set(city, int(old_value) + 1)
    else:
        rdb.set(city, 1)


def sortFunction(value):
	return value["number_of_visits"]


def read_from_database():
    result = []
    one_item = {}
    keys = rdb.keys()
    for key in keys:
        one_item['city'] = str(key)
        one_item['number_of_visits'] = int(rdb.get(key))
        result.append(one_item)
        one_item = {}
    sortedVisits = sorted(result, key=sortFunction, reverse=True)
    return sortedVisits
