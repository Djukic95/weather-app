from pyramid.view import view_config


@view_config(route_name='home', renderer='weather_app:templates/mytemplate.jinja2')
def my_view(request):
    return {'project': 'Weather App'}
