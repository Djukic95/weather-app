from pyramid.request import Request
from pyramid.response import Response
from pyramid.view import view_config
from weather_app.data.city_db import store_in_database, read_from_database
import json

import requests

LOCATION_IQ_API_KEY = "pk.3013a397591722a280c7fd59604bb33c"
LOCATION_IQ_ENDPOINT = "https://us1.locationiq.com/v1/reverse.php?key=" + LOCATION_IQ_API_KEY

WEATHER_API_KEY = "40252dd1fff609a23223cdc8cf6a1dbd"
WEATHER_API_ENDPOINT = "http://api.openweathermap.org/data/2.5/weather"
WEATHER_API_HOURLY_ENDPOINT = "http://api.openweathermap.org/data/2.5/forecast"


@view_config(route_name='get_weather',
             request_method='GET',
             renderer='json')
def get_weather(request: Request):

    data_type = request.headers['data-type']
    city = ''
    store_in_db = True
    if (data_type == "coordinates") :
        latitude = request.headers["latitude"]
        longitude = request.headers["longitude"]
        location_request_url = LOCATION_IQ_ENDPOINT + "&lat=" + latitude + "&lon=" + longitude + "&format=json"
        location_request = requests.get(location_request_url)
        store_in_db = False
        if (location_request.status_code == 200):
            city = location_request.json()['address']['city']
    else:
        city = request.headers["city"]

    return Response(status=200, json_body=get_weather_for_city(city, store_in_db))


def get_weather_for_city(city, store_in_db):
    weather_request_url = WEATHER_API_HOURLY_ENDPOINT + "?q=" + city +"&appid=" + WEATHER_API_KEY + "&units=metric"
    weather_request = requests.get(weather_request_url)
    list_hourly = []
    weather_info = {}
    if (weather_request.status_code == 200):
        result = weather_request.json()
        for r in result['list']:
            weather_info['city'] = result['city']['name']
            weather_info['description'] = r['weather'][0]['main']
            weather_info['icon'] = r['weather'][0]['icon']
            weather_info['curr_temp'] = r['main']['temp']
            weather_info['temp_min'] = r['main']['temp_min']
            weather_info['temp_max'] = r['main']['temp_max']
            time_tmp = r['dt_txt'].split(' ')
            hourly = time_tmp[1]
            hourly_tmp = hourly.split(':')
            weather_info['time'] = hourly_tmp[0] + ":" + hourly_tmp[1]
            list_hourly.append(weather_info)
            weather_info = {}
        if (store_in_db):
            store_in_database(city)
    return list_hourly


@view_config(route_name='get_visits',
             request_method='GET',
             renderer='json')
def get_visits(request: Request):
    return Response(status=200, json_body=read_from_database())