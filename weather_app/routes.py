def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('get_weather', '/api/get_weather')
    config.add_route('get_visits', '/api/get_visits')
