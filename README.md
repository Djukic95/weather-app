# WeatherApp

## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Running](#running)

## General info
This project is simple weather app which displays the weather forecast for the entered city.
	
## Technologies
Project is created with:
* Pyramid Framework
* Redis DB
	
## Running

Running server and redis.
```
$ execute 'redis\redis-server.exe' in cmd
$ execute 'env\Scripts\pserve.exe development.ini' in other cmd
```

Open browser and visit: localhost:6543

